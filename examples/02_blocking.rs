#[macro_use]
extern crate serde_derive;
extern crate docopt;
extern crate wanikani;

use docopt::Docopt;
use wanikani::blocking::WaniKaniClient;

// Just some boilerplate to read the API key
pub const USAGE: &'static str = "
WaniKani API test client.

Usage:
  01_test --key=<key>
  01_test [options]

Options:
  -h, --help       Show this message
  -k, --key=<key>  The WaniKani API key
";

#[derive(Debug, Deserialize)]
struct Args {
    pub flag_key: String,
}

fn main() {
    let args : Args = Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    let wk = WaniKaniClient::configure(args.flag_key.to_owned());

    if let Ok(user) = wk.user() {
        println!("{:#?}", user);
    } else {
        println!("Failed to get user");
    };

    if let Ok(subjects1) = wk.subjects(|f| f.levels(&[1, 2, 3]).types(&["kanji", "vocabulary", "radical"]))
    {
        println!("{:#?}", subjects1);
    };

    if let Ok(assignment1) = wk.assignment(88214909) {
        println!("{:#?}", assignment1);
    };
}
