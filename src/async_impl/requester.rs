use async_trait::async_trait;
use reqwest::{Client as ReqwestClient};
use serde::de::DeserializeOwned;
use crate::{Error, Result, BASE_URL};
use crate::filters::Filter;

#[async_trait]
pub trait WaniKaniRequester {
    async fn api_request<T, F>(&self, api_key: &String, resource: String, filter: F) -> Result<T>
        where T: DeserializeOwned,
              F: Filter + std::marker::Sync + std::marker::Send;
}

#[async_trait]
impl WaniKaniRequester for ReqwestClient {
    async fn api_request<T, F>(&self, api_key: &String, resource: String, filter: F) -> Result<T>
        where T: DeserializeOwned,
              F: Filter + std::marker::Sync + std::marker::Send
    {
        let res = self
            .get(format!("{}/{}", BASE_URL, resource))
            .bearer_auth(api_key)
            .query(&filter)
            .send().await?;
        res.json().await.map_err(Error::from)
    }
}