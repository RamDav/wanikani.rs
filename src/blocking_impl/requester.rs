use reqwest::blocking::{Client as ReqwestClient};
use serde::de::DeserializeOwned;

use crate::{Error, Result, BASE_URL};
use crate::filters::Filter;

pub trait WaniKaniRequester {
    fn api_request<T, F>(&self, api_key: &String, resource: String, filter: F) -> Result<T>
        where T: DeserializeOwned,
              F: Filter;
}

impl WaniKaniRequester for ReqwestClient {
    fn api_request<T, F>(&self, api_key: &String, resource: String, filter: F) -> Result<T>
        where T: DeserializeOwned,
              F: Filter
    {
        let res = self
            .get(format!("{}/{}", BASE_URL, resource))
            .bearer_auth(api_key)
            .query(&filter)
            .send()?;
        res.json().map_err(Error::from)
    }
}