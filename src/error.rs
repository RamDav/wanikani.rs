
use std::result;
use serde_json::Error as JSONError;
use reqwest::Error as ReqwestError;

#[derive(Debug)]
pub enum Error {
    JSONError(JSONError),
    RetrievalError(ReqwestError)
}

impl From<JSONError> for Error {
    fn from(error: JSONError) -> Self {
        Error::JSONError(error)
    }
}

impl From<ReqwestError> for Error {
    fn from(error: ReqwestError) -> Self {
        Error::RetrievalError(error)
    }
}

pub type Result<T> = result::Result<T, Error>;
