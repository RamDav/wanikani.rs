#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
extern crate serde_urlencoded;
extern crate chrono;
extern crate reqwest;

mod test_helpers;
mod error;

mod async_impl;
mod models_impl;

pub mod filters;

pub use error::Error;
pub use error::Result;
pub mod models {
    pub use super::models_impl::prelude::*;
}

pub use async_impl::Client as WaniKaniClient;

#[cfg(feature = "blocking")]
mod blocking_impl;

#[cfg(feature = "blocking")]
pub mod blocking {
    pub use super::blocking_impl::{Client as WaniKaniClient, WaniKaniRequester};
}

pub const BASE_URL: &'static str = "https://api.wanikani.com/v2";
