use super::Time;
use super::SubjectType;

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Assignment {
    pub created_at:     Time,
    pub subject_id:     u32,
    pub subject_type:   SubjectType,
    pub srs_stage:      u8,
    pub unlocked_at:    Option<Time>,
    pub started_at:     Option<Time>,
    pub passed_at:      Option<Time>,
    pub burned_at:      Option<Time>,
    pub available_at:   Option<Time>,
    pub resurrected_at: Option<Time>,
    pub hidden : bool
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_helpers::{date, maybe_date, fixture};
    use crate::models_impl::{Report, ObjectType};
    use serde_json;

    #[test]
    fn parse_assignment() {
        let file = fixture("assignment");
        let assignment: Report<Assignment> = serde_json::from_reader(file).unwrap();

        assert_eq!(
            assignment,
            Report {
                id: Some(309752395),
                object: ObjectType::Assignment,
                url: "https://api.wanikani.com/v2/assignments/309752395".to_string(),
                data_updated_at: maybe_date("2022-10-24T08:02:23.174158Z"),
                data: Assignment {
                    created_at: date("2022-10-09T08:07:22.738389Z"),
                    subject_id: 2715,
                    subject_type: SubjectType::Vocabulary,
                    srs_stage: 3,
                    unlocked_at: maybe_date("2022-10-09T08:07:22.731776Z"),
                    started_at: maybe_date("2022-10-09T10:02:29.759390Z"),
                    passed_at: maybe_date("2022-10-17T07:18:09.845015Z"),
                    burned_at: None,
                    available_at: maybe_date("2022-10-25T07:00:00.000000Z"),
                    resurrected_at: None,
                    hidden: false
                }
            }
        );
    }
}

pub mod prelude {
    pub use super::Assignment;
}