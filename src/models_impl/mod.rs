use chrono::prelude::{DateTime, Utc};

mod assignment;
mod level_progression;
mod reset;
mod review_statistic;
mod review;
mod study_material;
mod subject;
mod summary;
mod user;

pub type Time = DateTime<Utc>;

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Report<T> {
    pub id:              Option<u32>,
    pub object:          ObjectType,
    pub url:             String,
    pub data_updated_at: Option<Time>,
    pub data:            T,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ObjectType {
    User,
    Kanji,
    Vocabulary,
    Radical,
    Assignment,
    ReviewStatistic,
    StudyMaterial,
    Summary,
    Review,
    LevelProgression,
    Report,
    Reset,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum SubjectType {
    Kanji,
    Vocabulary,
    Radical,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Collection<T> {
    pub object:          String,
    pub url:             String,
    pub pages:           Pages,
    pub total_count:     u32,
    pub data_updated_at: Option<Time>,
    pub data:            Vec<Report<T>>,
}

impl<T> Collection<T> {
    pub fn to_slice(&self) -> &[Report<T>] {
        &self.data[..]
    }
}

impl<T> IntoIterator for Collection<T> {
    type Item = Report<T>;
    type IntoIter = ::std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.data.into_iter()
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct Pages {
    pub per_page:     u32,
    pub next_url:     Option<String>,
    pub previous_url: Option<String>
}

pub mod prelude {
    pub use super::assignment::Assignment;
    pub use super::level_progression::LevelProgression;
    pub use super::reset::Reset;
    pub use super::review_statistic::ReviewStatistic;
    pub use super::review::Review;
    pub use super::study_material::StudyMaterial;
    pub use super::subject::{
        AuxiliaryMeaning, ListType,
        Meaning,
        Image, ContentType, ImageMetadata,
        Subject, Radical, Kanji, Vocabulary, 
        KanjiReadings, KanjiReading,
        VocabularyReading, ContextSentences,
        PronounciationAudio, PronounciationAudioMetaData,
    };
    pub use super::summary::{Summary, SummaryItem};
    pub use super::user::{
        SubscriptionType, Subscription,
        PresentationOrder, Preferences,
        User
    };
    pub use super::{
        Time,
        Collection, Pages, Report, 
        ObjectType, SubjectType};
}
