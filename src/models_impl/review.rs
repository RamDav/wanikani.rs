use super::Time;

#[derive(Clone, Debug, Deserialize)]
pub struct Review {
    pub created_at:                  Time,
    pub assignment_id:               u32,
    pub spaced_repetition_system_id: u32,
    pub subject_id:                  u32,
    pub starting_srs_stage:          u32,
    pub ending_srs_stage:            u32,
    pub incorrect_meaning_answers:   u32,
    pub incorrect_reading_answers:   u32,
}
