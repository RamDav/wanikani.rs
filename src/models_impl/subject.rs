use super::Time;

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct AuxiliaryMeaning {
    #[serde(alias = "type")]
    pub list_type: ListType,
    pub meaning: String,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ListType {
    Whitelist ,
    Blacklist,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(untagged)]
pub enum ImageMetadata {
    Image {
        color : String,
        dimensions : String,
        style_name : String
    },
    SVG {
        inline_styles : bool
    }
}
#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Image  {
    url : String,
    metadata : ImageMetadata,
    content_type : ContentType,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub enum ContentType {
    #[serde(alias="image/svg+xml")]
    ImageSvgXml,
    #[serde(alias="image/png")]
    ImagePng,
    #[serde(alias="audio/mpeg")]
    AudioMpeg,
    #[serde(alias="audio/ogg")]
    AudioOgg,
    #[serde(alias="audio/webm")]
    AudioWebM,
}
#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Radical {
    pub auxiliary_meanings: Vec<AuxiliaryMeaning>,
    pub characters: Option<String>,
    pub created_at: Time,
    pub document_url: String,
    pub hidden_at : Option<Time>,
    pub lesson_position : i32,
    pub level: u8,
    pub meaning_mnemonic : String,
    pub meanings: Vec<Meaning>,
    pub slug: String,
    pub spaced_repetition_system_id: u32,
    pub amalgamation_subject_ids : Vec<u32>,
    pub character_images: Vec<Image>,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Kanji {
    pub auxiliary_meanings: Vec<AuxiliaryMeaning>,
    pub characters: String,
    pub created_at: Time,
    pub document_url: String,
    pub hidden_at : Option<Time>,
    pub lesson_position : i32,
    pub level: u8,
    pub meaning_mnemonic : String,
    pub meanings: Vec<Meaning>,
    pub slug: String,
    pub spaced_repetition_system_id: u32,
    pub amalgamation_subject_ids: Vec<u32>,
    pub component_subject_ids: Vec<u32>,
    pub meaning_hint : Option<String>,
    pub readings: Vec<KanjiReadings>,
    pub reading_mnemonic: String,
    pub reading_hint: Option<String>,
    pub visually_similar_subject_ids: Vec<u32>,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct  KanjiReading {
    pub primary : bool,
    pub reading : String,
    pub accepted_answer : bool,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum KanjiReadings {
    Onyomi  (KanjiReading),
    Kunyomi (KanjiReading),
    Nanori  (KanjiReading),
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Vocabulary {
    pub auxiliary_meanings: Vec<AuxiliaryMeaning>,
    pub characters: String,
    pub created_at: Time,
    pub document_url: String,
    pub hidden_at : Option<Time>,
    pub lesson_position : i32,
    pub level: u8,
    pub meaning_mnemonic : String,
    pub meanings: Vec<Meaning>,
    pub slug: String,
    pub spaced_repetition_system_id: u32,
    pub component_subject_ids: Vec<u32>,
    pub context_sentences: Vec<ContextSentences>,
    pub parts_of_speech: Vec<String>,
    pub pronunciation_audios: Vec<PronounciationAudio>,
    pub readings: Vec<VocabularyReading>,
    pub reading_mnemonic : String,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct ContextSentences {
    pub en : String,
    pub ja : String,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct PronounciationAudio {
    pub url : String,
    pub metadata: PronounciationAudioMetaData,
    pub content_type: ContentType,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct PronounciationAudioMetaData{
    pub gender: String,
    pub source_id: u32,
    pub pronunciation: String,
    pub voice_actor_id : u32,
    pub voice_actor_name : String,
    pub voice_description : String,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct VocabularyReading {
    primary: bool,
    reading: String,
    accepted_answer: bool,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(untagged)]
pub enum Subject {
    Kanji(Kanji),
    Vocabulary(Vocabulary),
    Radical(Radical),
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Meaning {
    pub meaning: String,
    pub primary: bool,
    pub accepted_answer: bool,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_helpers::{date, maybe_date, fixture};
    use crate::models::{Report, ObjectType};
    use serde_json;

    #[test]
    fn parse() {
        let file = fixture("subject");
        let report: Report<Subject> = serde_json::from_reader(file).unwrap();

        assert_eq!(
            Report {
                id: Some(1000),
                object: ObjectType::Kanji,
                url: "https://api.wanikani.com/v2/subjects/1000".to_string(),
                data_updated_at: maybe_date("2022-06-01T14:12:05.402958Z"),
                data: Subject::Kanji(Kanji {
                    created_at: date("2012-10-30T19:49:28.123286Z"),
                    level: 17,
                    slug: "\u{5175}".to_string(),
                    hidden_at: None,
                    document_url: "https://www.wanikani.com/kanji/%E5%85%B5".to_string(),
                    characters: "\u{5175}".to_string(),
                    meanings: vec![
                        Meaning {
                            meaning: "Soldier".to_string(),
                            primary: true,
                            accepted_answer: true
                        }
                    ],
                    auxiliary_meanings: vec![],
                    readings: vec![
                        KanjiReadings::Onyomi( KanjiReading { 
                            primary: true,
                            reading: "\u{3078}\u{3044}".to_string(),
                            accepted_answer: true
                        }),
                        KanjiReadings::Onyomi( KanjiReading {
                            primary: true,
                            reading: "\u{3072}\u{3087}\u{3046}".to_string(),
                            accepted_answer: true
                        })
                    ],
                    component_subject_ids: vec![
                        115,
                        1,
                        2
                    ],
                    amalgamation_subject_ids: vec![
                        3960,
                        3961,
                        3962,
                        3963,
                        4347,
                        5211,
                        5872,
                        5990,
                        6485,
                        7194,
                        7546,
                        8132,
                        8183
                    ],
                    visually_similar_subject_ids: vec![
                        1855,
                        1888,
                        1376
                    ],
                    meaning_mnemonic: "If you set a fish on the <radical>ground</radical> and chop its <radical>fins</radical> off with an <radical>axe</radical>, you\u{2019}re probably a <kanji>soldier</kanji>. Only soldiers have access to axes, and you like to eat fish, but first you have to set it on the ground to chop its fins off.".to_string(),
                    meaning_hint: Some("Imagine trying to carve a fish with an axe. Probably not an easy thing to do. If you\u{2019}re a trained axe soldier, you\u{2019}d probably have an easier time with it. Picture yourself chopping off fins as an expert axe soldier.".to_string()),
                    reading_mnemonic: "Uh-oh, it looks like this axe <kanji>soldier</kanji> was so excited about chopping the fins off his fish that he didn't think about how he was going to cook them. The only thing nearby to light on fire is a bale of <reading>hay</reading> (<ja>\u{3078}\u{3044}</ja>). Well, it's not the best thing, but at least it will get these fish cooked.".to_string(),
                    reading_hint: Some("Imagine this soldier lighting a bale of hay on fire to cook his fish. The hay burns really fast, but it should be enough.".to_string()),
                    lesson_position: 15,
                    spaced_repetition_system_id: 1
                })
            },
            report
        );
    }
}

