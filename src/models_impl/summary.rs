use super::Time;

#[derive(Clone, Debug, Deserialize)]
pub struct Summary {
    pub lessons: Vec<SummaryItem>,
    pub next_reviews_at : Time,
    pub reviews: Vec<SummaryItem>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct SummaryItem {
    pub available_at: Time,
    pub subject_ids:  Vec<u32>,
}
