use super::Time;


#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum SubscriptionType{
    Free,
    Recurring,
    Lifetime,
}
#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Subscription {
    pub active: bool,
    #[serde(alias = "type")]
    pub subscription_type: SubscriptionType,
    pub max_level_granted: u8,
    pub period_ends_at: Time,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum PresentationOrder{
    Shuffled,
    AscendingLevelThenSubject,
    AscendingLevelThenShuffled,
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Preferences {
    pub default_voice_actor_id: u32,
    pub lessons_autoplay_audio: bool,
    pub lessons_batch_size : u32,
    pub lessons_presentation_order : PresentationOrder,
    pub reviews_presentation_order : PresentationOrder,
    pub wanikani_compatibility_mode : bool,
    pub reviews_autoplay_audio: bool,
    pub extra_study_autoplay_audio : bool,
    pub reviews_display_srs_indicator: bool,
}
#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct User {
    pub id:                                String,
    pub username:                          String,
    pub level:                             u8,
    pub profile_url:                       String,
    pub started_at:                        Time,
    pub current_vacation_started_at:       Option<Time>,
    pub subscription:                      Subscription,
    pub preferences: Preferences,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_helpers::{date, maybe_date, fixture};
    use crate::models::{Report, ObjectType};
    use serde_json;

    #[test]
    fn parse() {
        let file = fixture("user");
        let user: Report<User> = serde_json::from_reader(file).unwrap();

        assert_eq!(
            user,
            Report {
                id: None,
                object: ObjectType::User,
                url: "https://api.wanikani.com/v2/user".to_string(),
                data_updated_at: maybe_date("2022-10-24T20:24:35.172927Z"),
                data: User {
                    id: "fage40ce-8cce-4655-821a-f2d792b7cb00".to_string(),
                    username: "example_user".to_string(),
                    level: 6,
                    profile_url: "https://www.wanikani.com/users/example_user".to_string(),
                    started_at: date("2022-01-01T13:37:35.536200Z"),
                    subscription: Subscription {
                        active: true,
                        subscription_type: SubscriptionType::Recurring,
                        max_level_granted: 60,
                        period_ends_at: date("2022-12-22T00:00:00.000000Z"),
                    },
                    current_vacation_started_at: None,
                    preferences: Preferences {
                        lessons_batch_size: 5,
                        default_voice_actor_id: 1,
                        lessons_autoplay_audio: false,
                        reviews_autoplay_audio: false,
                        extra_study_autoplay_audio: false,
                        lessons_presentation_order: PresentationOrder::AscendingLevelThenSubject,
                        reviews_presentation_order: PresentationOrder::Shuffled,
                        wanikani_compatibility_mode: false,
                        reviews_display_srs_indicator: true
                    }
                },
            }
        );
    }
}
